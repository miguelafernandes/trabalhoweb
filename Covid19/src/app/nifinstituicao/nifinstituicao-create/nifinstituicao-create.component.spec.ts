import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NifinstituicaoCreateComponent } from './nifinstituicao-create.component';

describe('NifinstituicaoCreateComponent', () => {
  let component: NifinstituicaoCreateComponent;
  let fixture: ComponentFixture<NifinstituicaoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NifinstituicaoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NifinstituicaoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
