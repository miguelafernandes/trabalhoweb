import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray, FormArrayName, FormControlDirective, FormControl} from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { NifInstituicao } from '../nifinstituicao.model';
import { NifInstituicaoService } from '../nifinstituicao.service';
import {Router} from '@angular/router';
import { element } from 'protractor';

//https://medium.com/@joshblf/using-child-components-in-angular-forms-d44e60036664
//https://www.digitalocean.com/community/tutorials/angular-reactive-forms-formarray-dynamic-fields
//https://www.positronx.io/angular-8-radio-buttons-tutorial-with-examples/?fbclid=IwAR39jE27GqtXa7R71NnLIhvfEJ0VcdTxU63yhm-NXZmAc1w-KUOAWvshUkM

@Component({
  selector: 'app-nifinstituicao-create',
  templateUrl: './nifinstituicao-create.component.html',
  styleUrls: ['./nifinstituicao-create.component.css']
})

export class NifinstituicaoCreateComponent implements OnInit {

  addCovidInstituicao: FormGroup
  memberList: FormArray

  nifInstituicao: string
  private mode = 'create'
  isLoading = false
  nifIndividuals: NifInstituicao
  private id: string
  router: Router

  constructor(public fb:FormBuilder, public nifInstituicaoService: NifInstituicaoService, public route: ActivatedRoute) {
    this.memberList = null
   }


  // criar membro do array
  createMemberSerialize(element: any): void {

    var newElement =  this.fb.group({
      nif: [element.nif, Validators.required],
      option: [element.option, Validators.required],
    })
    this.memberList = this.addCovidInstituicao.get('memberList') as FormArray;
    this.memberList.push(newElement)
  } 
 
  // inifiar form builder
  ngOnInit(): void {

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('id')) {

        this.addCovidInstituicao = this.fb.group({
          nif: new FormControl(this.nifInstituicao, [Validators.required, Validators.minLength(9)]),
          memberList: this.fb.array([])
        });

        
        this.mode = 'edit';
        this.id = paramMap.get('id');
        this.isLoading = true;
        this.nifInstituicaoService.getNifInstitucicao(this.id).subscribe(itemData => {
          this.isLoading = false;

          console.log("cenas")
          console.log(itemData)


          itemData.elementsList.forEach(element => {this.createMemberSerialize(element)})


          this.addCovidInstituicao.setValue({
            nif: itemData.nif,
            memberList: this.memberList
          })
        });
      }
      else {
        
        this.addCovidInstituicao = this.fb.group({
          nif: new FormControl(this.nifInstituicao, [Validators.required, Validators.minLength(9)]),
          memberList: this.fb.array([this.createMember()])
        });

        this.mode = 'create';
        this.id = null;
      }
    })
  }

  // criar membro do array
  createMember(): FormGroup {
    return this.fb.group({
      nif: [null, Validators.required],
      option: [null, Validators.required],
    });
  }


  // add novo membro
  addNewMember(): void {
    this.memberList = this.addCovidInstituicao.get('memberList') as FormArray;
    this.memberList.push(this.createMember());
  }

  // botão submeter formulário
  onSubmit(): void {

    if (!this.addCovidInstituicao.valid) {
      console.log(JSON.stringify(this.addCovidInstituicao.value ))
      return;
    } else {
      const nifObj : NifInstituicao = {
        _id: '',
        nif : this.addCovidInstituicao.value.nif,
        elementsList : this.addCovidInstituicao.value.memberList
      }

      this.isLoading = true;

      if(this.mode === 'create'){

        if (this.validateNIF(nifObj.nif))
        {
          this.nifInstituicaoService.addNifInstitucicao(nifObj.nif, nifObj.elementsList)
          this.addCovidInstituicao.reset()
        }
        else {
          alert("Nif inválido")
        }
      
      } else {
        if (this.validateNIF(nifObj.nif))
        {
          this.nifInstituicaoService.updateNifInstitucicao(
            this.id,
            nifObj.nif,
            nifObj.elementsList
          );
        }
        else {
          alert("Nif inválido")
        }
      }
      console.log(JSON.stringify(this.addCovidInstituicao.value));
    }
  }

  // validate nif
  validateNIF(nif){
    var control
    if(nif.length == 9){
        var added = ((nif[7]*2)+(nif[6]*3)+(nif[5]*4)+(nif[4]*5)+(nif[3]*6)+(nif[2]*7)+(nif[1]*8)+(nif[0]*9));
        var mod = added % 11;
        if(mod == 0 || mod == 1){
          control = 0;
        } else {
             control = 11 - mod;   
        }
        
        if(nif[8] == control){
            return true;
        } else {
             return false;  
        }
    } else {
        return false;
    }
  }
}
