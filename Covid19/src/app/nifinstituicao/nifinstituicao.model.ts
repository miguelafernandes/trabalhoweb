
import {NifIndividual} from '../nifindividual/nifindividual.model'

export interface NifInstituicao {
    _id: string,
    nif: string, 
    elementsList: any
}