import { NifIndividual} from '../nifindividual/nifindividual.model';
import { NifInstituicao } from './nifinstituicao.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})
export class NifInstituicaoService{
    private nifInstituicoes: NifInstituicao[] = [];
    private nifInstituicoesUpdated = new Subject<{nifInstituicoes: NifInstituicao[], nifInstituicaoCount: number}>();

    constructor(private httpClient: HttpClient, private router: Router){}

    getNifInstitucicao(nifId: string) {
      return this.httpClient.get<{nif: string, elementsList: []}>("http://localhost:3000/api/instituicoes/"+nifId);
    }

    getNifInstitucicoes(elementsPerPage: number, currentPage: number) {
        const queryParams = `?pageSize=${elementsPerPage}&page=${currentPage}`;


        this.httpClient.get<{message:string, instituicoes: any, maxInstituicoes: number}>(
          'http://localhost:3000/api/instituicoes' + queryParams)
        .pipe(
            map(instituicaoData => {
              return {
                institucicoes: instituicaoData.instituicoes.map(instituicao => {
                  return {
                    nif: instituicao.nif,
                    elementsList: instituicao.elementsList.map(element => { return { nif: element.nif, option: element.option}}),
                    _id: instituicao._id
                  };
                }),
                maxInstituicoes: instituicaoData.maxInstituicoes
              };
            })
          )
        .subscribe((instituicaoData) => {
            this.nifInstituicoes = instituicaoData.institucicoes;
            this.nifInstituicoesUpdated.next({nifInstituicoes: [...this.nifInstituicoes], nifInstituicaoCount: instituicaoData.maxInstituicoes});
        });
    }

    getNifInstitucicaoUpdateListener() {
        return this.nifInstituicoesUpdated.asObservable();
    }

    addNifInstitucicao(nif: string, elementsList: []) {
        const newNif: NifInstituicao = {
          _id: "",
          nif: nif,
          elementsList: elementsList
        }

        console.log("o caralho do nif")
        console.log(newNif)

        this.httpClient
        .post<{ message: string, nifInstituicaoId: string }>('http://localhost:3000/api/instituicoes', newNif)
        .subscribe((responseData) => {
            this.router.navigate(["/nifinstituicao"]);
        });
    }

    updateNifInstitucicao(id:string, nif: string, elementsList: []) {
      const newNif: NifInstituicao = {_id: id, nif: nif, elementsList: elementsList };
      this.httpClient
      .put("http://localhost:3000/api/instituicoes/" + id, newNif)
      .subscribe(response => {
        this.router.navigate(["/nifinstituicao"]);
      });
    }

    deleteNifInstitucicao(id: string) {
        return this.httpClient
        .delete<{message: string}>('http://localhost:3000/api/instituicoes/' + id);
    }
}