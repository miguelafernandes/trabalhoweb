import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NifinstituicaoListComponent } from './nifinstituicao-list.component';

describe('NifinstituicaoListComponent', () => {
  let component: NifinstituicaoListComponent;
  let fixture: ComponentFixture<NifinstituicaoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NifinstituicaoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NifinstituicaoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
