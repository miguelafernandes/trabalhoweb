import { Component, OnInit } from '@angular/core';
import { NifInstituicao } from '../nifinstituicao.model';
import { NifInstituicaoService } from 'src/app/nifinstituicao/nifinstituicao.service';
import { PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-nifinstituicao-list',
  templateUrl: './nifinstituicao-list.component.html',
  styleUrls: ['./nifinstituicao-list.component.css']
})
export class NifinstituicaoListComponent implements OnInit {

  private instituicoesSub : Subscription
  isLoading = false;
  totalInstituicoes = 0;
  instituicoesPerPage = 2;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];
  instituicoes: NifInstituicao[] = [];

  constructor( public instotiocoesService: NifInstituicaoService ) { }

  ngOnInit(): void {

    this.isLoading = true;
    this.instotiocoesService.getNifInstitucicoes(this.instituicoesPerPage, this.currentPage);

    this.instituicoesSub = this.instotiocoesService.getNifInstitucicaoUpdateListener().subscribe( (instituicaoData: { nifInstituicoes: NifInstituicao[]; nifInstituicaoCount: number;}) => {
        this.isLoading = false;
        this.totalInstituicoes = instituicaoData.nifInstituicaoCount;
        this.instituicoes = instituicaoData.nifInstituicoes;
    });
  }

  ngOnDestroy(){
    this.instituicoesSub.unsubscribe(); 
  }

  onDelete(id: string){
    this.instotiocoesService.deleteNifInstitucicao(id).subscribe(() => {
      this.instotiocoesService.getNifInstitucicoes(this.instituicoesPerPage, this.currentPage);
    });
  }

  onChangedPage(pageDate: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageDate.pageIndex + 1;
    this.instituicoesPerPage = pageDate.pageSize;
    this.instotiocoesService.getNifInstitucicoes(this.instituicoesPerPage, this.currentPage);
  }
}
