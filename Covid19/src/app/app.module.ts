import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NifindividualCreateComponent } from './nifindividual/nifindividual-create/nifindividual-create.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NifinstituicaoCreateComponent } from './nifinstituicao/nifinstituicao-create/nifinstituicao-create.component';
import { NavbarComponent } from './navbar/navbar.component';
import { Page404Component } from './page404/page404.component';
import { HomeComponent } from './home/home.component';
import { NifindividualListComponent } from './nifindividual/nifindividual-list/nifindividual-list.component';
import { NifinstituicaoListComponent } from './nifinstituicao/nifinstituicao-list/nifinstituicao-list.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ChartComponent } from './chart/chart.component';


@NgModule({
  declarations: [
    AppComponent,
    NifindividualCreateComponent,
    NifinstituicaoCreateComponent,
    NavbarComponent,
    Page404Component,
    HomeComponent,
    NifindividualListComponent,
    NifinstituicaoListComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule, 
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatExpansionModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
