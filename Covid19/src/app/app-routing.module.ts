import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { Page404Component } from './page404/page404.component';
import { NifindividualListComponent } from './nifindividual/nifindividual-list/nifindividual-list.component';
import { NifinstituicaoListComponent } from './nifinstituicao/nifinstituicao-list/nifinstituicao-list.component';
import { NifindividualCreateComponent } from './nifindividual/nifindividual-create/nifindividual-create.component';
import { NifinstituicaoCreateComponent } from './nifinstituicao/nifinstituicao-create/nifinstituicao-create.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'nifindividual', component: NifindividualListComponent },
  {path: 'nifindividual/create', component: NifindividualCreateComponent },
  {path: 'nifindividual/edit/:id', component: NifindividualCreateComponent },
  {path: 'nifinstituicao', component: NifinstituicaoListComponent },
  {path: 'nifinstituicao/create', component: NifinstituicaoCreateComponent },
  {path: 'nifinstituicao/edit/:id', component: NifinstituicaoCreateComponent },
  { path: '**', component: Page404Component }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }