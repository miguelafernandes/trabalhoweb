import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NifindividualCreateComponent } from './nifindividual-create.component';

describe('NifindividualCreateComponent', () => {
  let component: NifindividualCreateComponent;
  let fixture: ComponentFixture<NifindividualCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NifindividualCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NifindividualCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
