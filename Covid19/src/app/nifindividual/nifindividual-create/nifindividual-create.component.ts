import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NifIndividualService } from '../nifindividual.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { NifIndividual } from '../nifindividual.model';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';

//https://medium.com/@joshblf/using-child-components-in-angular-forms-d44e60036664
//https://www.digitalocean.com/community/tutorials/angular-reactive-forms-formarray-dynamic-fields
//https://www.positronx.io/angular-8-radio-buttons-tutorial-with-examples/?fbclid=IwAR39jE27GqtXa7R71NnLIhvfEJ0VcdTxU63yhm-NXZmAc1w-KUOAWvshUkM

@Component({
  selector: 'app-nifindividual-create',
  templateUrl: './nifindividual-create.component.html',
  styleUrls: ['./nifindividual-create.component.css']
})

export class NifindividualCreateComponent implements OnInit, OnDestroy {
  
  private mode = 'create'
  addCovidIndividual: FormGroup
  isLoading = false
  nifIndividuals: NifIndividual
  private id: string
  router: Router

  constructor(public fb:FormBuilder, public httpClient: HttpClient,  public nifIndividualService: NifIndividualService, public route: ActivatedRoute) { }

  ngOnInit(): void {

    this.addCovidIndividual = this.fb.group({
      nif: ['', Validators.required],
      option:['', Validators.required]
    });

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('id')) {
        this.mode = 'edit';
        this.id = paramMap.get('id');
        this.isLoading = true;
        this.nifIndividualService.getNifInidvidual(this.id).subscribe(itemData => {
          this.isLoading = false;
          console.log(itemData)
          this.addCovidIndividual.setValue({
            nif: itemData.nif,
            option: itemData.option
          })
          
        });
      }
      else {
        this.mode = 'create';
        this.id = null;
      }
    })
  }

  ngOnDestroy(): void {
    
  }

  onSubmit(){

    if (!this.addCovidIndividual.valid) {
      console.log(JSON.stringify(this.addCovidIndividual.value))
      return;
    } else {

      const nifObj : NifIndividual = {
        _id: '',
        nif : this.addCovidIndividual.value.nif,
        option : this.addCovidIndividual.value.option
      }

      this.isLoading = true;

      if(this.mode === 'create'){
        if(this.validateNIF(nifObj.nif)){
          this.nifIndividualService.addNifIndividual(nifObj.nif, nifObj.option)
          this.addCovidIndividual.reset()
        }
        else{
          alert("nif inválido")
        }        
      } else {
        if(this.validateNIF(nifObj.nif)){
          this.nifIndividualService.updateNifIndividual(
            this.id,
            nifObj.nif,
            nifObj.option
          );
        }
        else{
          alert("nif inválido")
        } 
      
      }

      console.log(this.addCovidIndividual.value)
    }
  }

  // com api nif.pt
  // validarNif(nif) {
  //   var test = this.httpClient.get("http://www.nif.pt/?json=1&q="+nif+"&key=ed24afc3c3e82050a23539b042431ca7");
  //   console.log(test)
  //   return test
  // }

  validateNIF(nif){
    var control
    if(nif.length == 9){
        var added = ((nif[7]*2)+(nif[6]*3)+(nif[5]*4)+(nif[4]*5)+(nif[3]*6)+(nif[2]*7)+(nif[1]*8)+(nif[0]*9));
        var mod = added % 11;
        if(mod == 0 || mod == 1){
          control = 0;
        } else {
             control = 11 - mod;   
        }
        
        if(nif[8] == control){
            return true;
        } else {
             return false;  
        }
    } else {
        return false;
    }
  }

  
}
