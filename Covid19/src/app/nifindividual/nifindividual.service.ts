import { NifIndividual } from './nifindividual.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})
export class NifIndividualService{
    private nifinidviduals: NifIndividual[] = [];
    private nifinidvidualsUpdated = new Subject<{nifinidviduals: NifIndividual[], nifinidvidualCount: number}>();

    constructor(private httpClient: HttpClient, private router: Router){}

    getNifInidvidual(nifId: string) {
      return this.httpClient.get<{nif: string, option: string}>("http://localhost:3000/api/individuals/"+nifId);
    }

    getNifInidviduals(elementsPerPage: number, currentPage: number) {
        const queryParams = `?pageSize=${elementsPerPage}&page=${currentPage}`;


        this.httpClient.get<{message:string, individuals: any, maxIndividuals: number}>(
          'http://localhost:3000/api/individuals' + queryParams)
        .pipe(
            map(individualData => {
              return {
                individuals: individualData.individuals.map(individual => {
                  return {
                    nif: individual.nif,
                    option: individual.option,
                    _id: individual._id
                  };
                }),
                maxIndividuals: individualData.maxIndividuals
              };
            })
          )
        .subscribe((individualData) => {
            this.nifinidviduals = individualData.individuals;
            this.nifinidvidualsUpdated.next({nifinidviduals: [...this.nifinidviduals], nifinidvidualCount: individualData.maxIndividuals});
        });
    }

    getNifIndividualUpdateListener() {
        return this.nifinidvidualsUpdated.asObservable();
    }

    addNifIndividual(nif: string, option: string) {
        const newNif: NifIndividual = {
          _id: "",
          nif: nif,
          option: option
        }
        this.httpClient
        .post<{ message: string, postId: string }>('http://localhost:3000/api/individuals', newNif)
        .subscribe((responseData) => {
            this.router.navigate(["/nifindividual"]);
        });
    }

    updateNifIndividual(id:string, nif: string, option: string) {
      const newNif: NifIndividual = {_id: id, nif: nif, option: option };
      this.httpClient
      .put("http://localhost:3000/api/individuals/" + id, newNif)
      .subscribe(response => {
        this.router.navigate(["/nifindividual"]);
      });
    }

    deleteNifIndividual(id: string) {
        return this.httpClient
        .delete<{message: string}>('http://localhost:3000/api/individuals/' + id);
    }
}