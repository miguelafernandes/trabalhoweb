export interface NifIndividual {
    _id: string,
    nif: string, 
    option: string
}