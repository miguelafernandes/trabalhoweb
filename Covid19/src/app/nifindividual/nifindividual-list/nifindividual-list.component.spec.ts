import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NifindividualListComponent } from './nifindividual-list.component';

describe('NifindividualListComponent', () => {
  let component: NifindividualListComponent;
  let fixture: ComponentFixture<NifindividualListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NifindividualListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NifindividualListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
