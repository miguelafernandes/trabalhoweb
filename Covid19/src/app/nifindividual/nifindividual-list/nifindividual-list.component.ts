import { Component, OnInit } from '@angular/core';
import { NifIndividualService } from '../nifindividual.service';
import { Subscription } from 'rxjs';
import { NifIndividual } from '../nifindividual.model';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-nifindividual-list',
  templateUrl: './nifindividual-list.component.html',
  styleUrls: ['./nifindividual-list.component.css']
})
export class NifindividualListComponent implements OnInit {
  private individualsSub : Subscription

  isLoading = false;
  totalIndividuals = 0;
  individualsPerPage = 2;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];
  individuals: NifIndividual[] = [];

  constructor( public individualService: NifIndividualService ) { }

  ngOnInit() {

    this.isLoading = true;
    this.individualService.getNifInidviduals(this.individualsPerPage, this.currentPage);

    this.individualsSub = this.individualService.getNifIndividualUpdateListener().subscribe( (individualData: {nifinidviduals: NifIndividual[]; nifinidvidualCount: number;}) => {
        this.isLoading = false;
        this.totalIndividuals = individualData.nifinidvidualCount;
        this.individuals = individualData.nifinidviduals;
    });
  }

  ngOnDestroy(){
    this.individualsSub.unsubscribe(); 
  }

  onDelete(id: string){
    this.individualService.deleteNifIndividual(id).subscribe(() => {
      this.individualService.getNifInidviduals(this.individualsPerPage, this.currentPage);
    });
  }

  onChangedPage(pageDate: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageDate.pageIndex + 1;
    this.individualsPerPage = pageDate.pageSize;
    this.individualService.getNifInidviduals(this.individualsPerPage, this.currentPage);
  }
}
