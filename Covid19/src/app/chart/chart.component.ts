import { Component, OnInit } from '@angular/core';
import { NifIndividual } from '../nifindividual/nifindividual.model';
import { NifIndividualService } from '../nifindividual/nifindividual.service';
import * as CanvasJS from '../../assets/canvasjs.min';
import { Subscription } from 'rxjs';
import { element } from 'protractor';

@Component({
	selector: 'app-chart',
	templateUrl: './chart.component.html'
})
 
export class ChartComponent implements OnInit {
  private individualsSub : Subscription

  isLoading = false;
  totalIndividuals = 0;
  individualsPerPage = 10;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];
  individuals: NifIndividual[] = [];

  constructor( public individualService: NifIndividualService ) { }

  ngOnInit() {

    this.isLoading = true;
    this.individualService.getNifInidviduals(this.individualsPerPage, this.currentPage);

    this.individualsSub = this.individualService.getNifIndividualUpdateListener().subscribe( (individualData: {nifinidviduals: NifIndividual[]; nifinidvidualCount: number;}) => {
        this.isLoading = false;
        this.totalIndividuals = individualData.nifinidvidualCount;
        this.individuals = individualData.nifinidviduals;
        console.log(this.individuals)
        this.chartInit()
    });
  }

  chartInit(): void {
    let chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "Distribuição de estados"
      },
      data: [{
        type: "column",
        dataPoints: [
          { y: this.calculateValues(2), label: "Confirmados" },
          { y: this.calculateValues(0), label: "Suspeitos" },
          { y: this.calculateValues(1), label: "Não confirmados" },
          { y: this.calculateValues(3), label: "Recuperados" },
          { y: this.calculateValues(4), label: "Óbitos" }
        ]
      }]
	  });
		
	  chart.render();
  }


  calculateValues(type) : number {
    var sum = 0;
    this.individuals.forEach(element => { if (element.option == type) { sum++ }})
    return sum 
  }
}
