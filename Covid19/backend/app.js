const express = require('express');
const app = express()
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const individualRoutes = require('./routes/individual')
const instituicaoRoutes = require('./routes/instituicao')

// database connection with mongoose
mongoose.connect(
    "mongodb+srv://dev:dev123@cluster0-vwlle.mongodb.net/test?retryWrites=true&w=majority"
)
.then(()=>{
    console.log("connection successfull")
})
.catch((error)=>{
    console.log("Connection failed!: " + error)
})

// define usage of body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))

// set app header Acess control from Origin, Header and Methods
app.use((req, res, next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");

    next();
});

app.use("/api/individuals", individualRoutes)
app.use("/api/instituicoes", instituicaoRoutes)

module.exports = app;