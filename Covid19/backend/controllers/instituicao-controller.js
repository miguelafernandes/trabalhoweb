const Instituicao = require("../models/instituicao");

// metodo para cria caso instituicao
exports.createInstituicao = (req, res, next) => {
    const instituicao = new Instituicao({
        nif: req.body.nif,
        elementsList: req.body.elementsList
    });

    instituicao
    .save()
    .then( createdInstituicao => {
        res.status(201).json({
            message: 'Instituicao criado com sucesso',
            postId: createdInstituicao._id
        })
    })
    .catch((err) => {
        res.status(500).json({
            message: 'Erro ao criar Instituicao'
        })
    });
}

// metodo para recolher caso instituicao
exports.getInstituicao = (req, res, next) => {
    Instituicao.findById(req.params.id)
    .then(instituicao => {
        if(instituicao) {
            res.status(200).json(instituicao);
        } else {
            res.status(404).json({message: "instituicao não encontrado"});
        }
    })
    .catch(error => {
        console.log(error);
        res.status(500).json({
            message: "Falhou a procura de um caso instituicao"
        });
    });
}

// metodo para editar caso instituicao
exports.editInstituicao = (req, res, next) => {
    const newInstituicao = new Instituicao({
        _id: req.body._id,
        nif: req.body.nif,
        elementsList: req.body.elementsList
    })
    Instituicao.updateOne({ _id: req.params.id }, newInstituicao)
    .then(result => {
        if(result.n > 0) {
            res.status(200).json({message: "Update com sucesso"})
        } else {
            res.status(404).json({message: "Não foi possível atualizar"})
        }
    })
    .catch(error => {
        res.status(500).json({
            message: 'Erro ao atualizar Instituicao'
        })
    });
}

// metodo para recolher todos os caso Instituicao
exports.getAllInstituicoes = (req, res, next) => {
    const pageSize = +req.query.pageSize;
    const currentPage = +req.query.page;
    const instituicoesQuery = Instituicao.find();
    let fetchedInstituicoes;

    if(pageSize && currentPage) {
        instituicoesQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
    }
    instituicoesQuery
    .then(instituicoes => {
        fetchedInstituicoes = instituicoes;
        return Instituicao.count();
    })
    .then(count => {
        res.status(200).json({
            message: "Instituicoes enviados com sucesso",
            instituicoes: fetchedInstituicoes,
            maxInstituicoes: count
        });
    })
    .catch(error => {
        console.log(error);
        res.status(500).json({
            message: 'Erro ao fazer fetch de all instituicoes'
        })
    });;
}

// metodo para apagar caso Instituicao
exports.deleteInstituicao = (req, res, next) => {
 
    Instituicao.deleteOne({ _id: req.params.id })
    .then((result) => {
        res.status(200).json({message: "Instituicao deleted"});
    })
}