const Individual = require("../models/individual");

// metodo para cria caso individual
exports.createIndividual = (req, res, next) => {
    const individual = new Individual({
        nif: req.body.nif,
        option: req.body.option
    });

    individual
    .save()
    .then( createdIndividual => {
        res.status(201).json({
            message: 'Individual criado com sucesso',
            postId: createdIndividual._id
        })
    })
    .catch((err) => {
        res.status(500).json({
            message: 'Erro ao criar individual'
        })
    });
}

// metodo para recolher caso individual
exports.getIndividual = (req, res, next) => {
    Individual.findById(req.params.id)
    .then(individual => {
        if(individual) {
            res.status(200).json(individual);
        } else {
            res.status(404).json({message: "individual não encontrado"});
        }
    })
    .catch(error => {
        console.log(error);
        res.status(500).json({
            message: "Falhou a procura de um caso individual"
        });
    });
}

// metodo para editar caso individual
exports.editIndividual = (req, res, next) => {
    const newIndividual = new Individual({
        _id: req.body._id,
        nif: req.body.nif,
        option: req.body.option
    })
    Individual.updateOne({ _id: req.params.id }, newIndividual)
    .then(result => {
        if(result.n > 0) {
            res.status(200).json({message: "Update com sucesso"})
        } else {
            res.status(404).json({message: "Não foi possível atualizar"})
        }
    })
    .catch(error => {
        res.status(500).json({
            message: 'Erro ao atualizar individual'
        })
    });
}

// metodo para recolher todos os caso individual
exports.getAllIndividuals = (req, res, next) => {
    const pageSize = +req.query.pageSize;
    const currentPage = +req.query.page;
    const individualQuery = Individual.find();
    let fetchedIndividual;

    if(pageSize && currentPage) {
        individualQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
    }
    individualQuery
    .then(individuals => {
        fetchedIndividual = individuals;
        return Individual.count();
    })
    .then(count => {
        res.status(200).json({
            message: "Individuals enviados com sucesso",
            individuals: fetchedIndividual,
            maxIndividuals: count
        });
    })
    .catch(error => {
        console.log(error);
        res.status(500).json({
            message: 'Erro ao fazer fetch de all individuals'
        })
    });;
}

// metodo para apagar caso individual
exports.deleteIndividual = (req, res, next) => {
 
    Individual.deleteOne({ _id: req.params.id })
    .then((result) => {
        res.status(200).json({message: "Individual deleted"});
    })
}