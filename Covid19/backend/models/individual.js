
// individual model

const mongoose = require("mongoose");

const individualSchema = mongoose.Schema({
    nif: {type: String, required: true },
    option: {type: String, required: true}
});

module.exports = mongoose.model("Individual", individualSchema)

