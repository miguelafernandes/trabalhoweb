
// instituicao model

const mongoose = require("mongoose");

const instituicaoSchema = mongoose.Schema({
    nif: {type: String, required: true},
    elementsList: { type: Array, required: true}
});

module.exports = mongoose.model("Instituicao", instituicaoSchema)