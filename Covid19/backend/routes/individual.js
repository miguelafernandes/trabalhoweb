const express = require('express');
const router = express.Router();

const IndividualController = require("../controllers/individual-controller");

router.post("", IndividualController.createIndividual);
router.get("/:id", IndividualController.getIndividual);
router.put("/:id", IndividualController.editIndividual);
router.get("", IndividualController.getAllIndividuals);
router.delete("/:id", IndividualController.deleteIndividual);

module.exports = router;