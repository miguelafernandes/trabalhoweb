const express = require('express');
const router = express.Router();

const InstituicoesController = require("../controllers/instituicao-controller");

router.post("", InstituicoesController.createInstituicao);
router.get("/:id", InstituicoesController.getInstituicao);
router.put("/:id", InstituicoesController.editInstituicao);
router.get("", InstituicoesController.getAllInstituicoes);
router.delete("/:id", InstituicoesController.deleteInstituicao);

module.exports = router;